# Getting Started
1. Clone this project. `git clone git@bitbucket.org:byuhbll/payara-micro-demo.git`
2. Download the Payara Micro jar from http://www.payara.fish/downloads.
3. Build the project. `mvn clean package`
4. Run the application. `java -jar /path/to/payara-micro-4.1.1.163.jar --port 8081 --deploy target/payara-micro-demo-0.0.1.war`
5. See it at http://localhost:8081/pmd/hello .