/**
 * 
 */
package edu.byu.hbll.pmd;

import java.util.concurrent.atomic.AtomicInteger;

import javax.ejb.Schedule;
import javax.ejb.Singleton;

/**
 * 
 */
@Singleton
public class Scheduler {
    
    /**
     * 
     */
    private AtomicInteger count = new AtomicInteger();
    
    /**
     * 
     */
    @Schedule(second="*", minute="*", hour="*", persistent=false)
    public void timeout() {
        count.incrementAndGet();
    }
    
    /**
     * @return
     */
    public int getCount() {
        return count.get();
    }
    
}
