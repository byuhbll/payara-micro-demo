/**
 * 
 */
package edu.byu.hbll.pmd;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * 
 */
@Path("hello")
public class Service {
    
    /**
     * 
     */
    @Inject
    private Scheduler scheduler;
    
    /**
     * @return
     */
    @GET
    public String get() {
        return "hello world " + scheduler.getCount();
    }
    
    
}
